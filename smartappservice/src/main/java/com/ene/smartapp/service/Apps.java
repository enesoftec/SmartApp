package com.ene.smartapp.service;


import android.util.Base64;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by ENESOFTEC on 9/11/2016.
 */

public class Apps {

    private final String TAG = "Apps";



    private ArrayList<App> appArrayList;

    public Apps(){
        appArrayList = new ArrayList<>();
    }

    public boolean add(App app){
        for(App item : appArrayList){
            if(item.getLabel().toLowerCase().equals(app.getLabel().toLowerCase()) &&
                    item.getPackageName().toLowerCase().equals(app.getPackageName().toLowerCase())) {
                return false;
            }
        }
        appArrayList.add(app);
        return true;
    }

    public int sizeApps(){
        return appArrayList.size();
    }

    public App getApp(int index){
        return appArrayList.get(index);
    }

    public ArrayList<App> getApps(){
        return appArrayList;
    }

    public void setAppsFromString(String str){
        appArrayList.clear();
        String[] firstSplit = str.split(",");
        for(int i=0; i<firstSplit.length; i++){
            String[] secondSplit = firstSplit[i].split("#");
            if(secondSplit.length == 2)
                add(new App(secondSplit[0], secondSplit[1]));
        }
    }

    @Override
    public String toString() {
        String str = "";
        for(App item : appArrayList){
            str += item.getLabel() + "#" + item.getPackageName() + ",";
        }
        Log.d(TAG, str);
        return str;
    }
}
