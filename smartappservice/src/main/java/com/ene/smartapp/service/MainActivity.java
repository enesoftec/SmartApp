package com.ene.smartapp.service;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Intent intentService;
    private boolean ServiceActived = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        intentService = new Intent(this, EnesoftService.class);


        Button btnStartService = (Button) findViewById(R.id.btnStartService);
        btnStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ServiceActived){
                    startService(intentService);
                    System.out.println("Iniciando...");
                }else{
                    stopService(intentService);
                    System.out.println("Deteniendo...");
                }

                ServiceActived = !ServiceActived;



            }
        });
    }
}
