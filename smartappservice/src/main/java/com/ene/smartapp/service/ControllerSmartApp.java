package com.ene.smartapp.service;

/*
 * Copyright (C) 2013 4th Line GmbH, Switzerland
 *
 * The contents of this file are subject to the terms of either the GNU
 * Lesser General Public License Version 2 or later ("LGPL") or the
 * Common Development and Distribution License Version 1 or later
 * ("CDDL") (collectively, the "License"). You may not use this file
 * except in compliance with the License. See LICENSE.txt for more
 * information.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

        import android.graphics.Point;
        import android.graphics.PointF;

        import org.teleal.cling.binding.annotations.UpnpAction;
        import org.teleal.cling.binding.annotations.UpnpInputArgument;
        import org.teleal.cling.binding.annotations.UpnpOutputArgument;
        import org.teleal.cling.binding.annotations.UpnpService;
        import org.teleal.cling.binding.annotations.UpnpServiceId;
        import org.teleal.cling.binding.annotations.UpnpServiceType;
        import org.teleal.cling.binding.annotations.UpnpStateVariable;

        import java.beans.PropertyChangeSupport;

// DOC:CLASS
@UpnpService(
        serviceId = @UpnpServiceId("ControllerSmartApp"),
        serviceType = @UpnpServiceType(value = "ControllerSmartApp", version = 1)
)
public class ControllerSmartApp {

    private final PropertyChangeSupport propertyChangeSupport;

    public static Apps apps;

    public ControllerSmartApp() {
        this.propertyChangeSupport = new PropertyChangeSupport(this);
    }

    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    @UpnpStateVariable(defaultValue = "0")
    private String appsInstalleds = "";

    @UpnpStateVariable(defaultValue = "0")
    private String openApp = "";

    @UpnpStateVariable(defaultValue = "0")
    private String uninstallApp = "";


    @UpnpStateVariable(defaultValue = "0")
    private int coordenatePointer = 0;

    @UpnpStateVariable(defaultValue = "0")
    private boolean volumeMute = true;

    @UpnpStateVariable(defaultValue = "0")
    private boolean showNotification = true;

    @UpnpStateVariable(defaultValue = "0")
    private String inputKeyboard = "";


    @UpnpAction(out = @UpnpOutputArgument(name = "ResultAppsInstalleds"))
    public String getAppsInstalleds() {
        return EnesoftService.listAppsInstalleds();
    }

    @UpnpAction
    public void setOpenApp(@UpnpInputArgument(name = "NewOpenAppValue") String newOpenAppValue) {
        String oldValue = openApp;
        openApp = newOpenAppValue;

        getPropertyChangeSupport().firePropertyChange("open_app", oldValue, openApp);
    }

    @UpnpAction
    public void setUninstallApp(@UpnpInputArgument(name = "NewUninstallAppValue") String newUninstallAppValue) {
        String oldValue = uninstallApp;
        uninstallApp = newUninstallAppValue;

        getPropertyChangeSupport().firePropertyChange("uninstall_app", oldValue, uninstallApp);
    }

    @UpnpAction
    public void setCoordenatePointer(@UpnpInputArgument(name = "NewCoordenateX") int newCX,
                                     @UpnpInputArgument(name = "NewCoordenateY") int newCY) {
        Point point = new Point(newCX, newCY);

        coordenatePointer = newCX;

        getPropertyChangeSupport().firePropertyChange("move", null, point);
    }

    @UpnpAction
    public void clickPointer() {
        getPropertyChangeSupport().firePropertyChange("click", null, null);
    }


    @UpnpAction
    public void goMusic() {
        getPropertyChangeSupport().firePropertyChange("music", null, null);
    }

    @UpnpAction
    public void goMusicPlayPause() {
        getPropertyChangeSupport().firePropertyChange("media_play_pause", null, null);
    }

    @UpnpAction
    public void goMusicStop() {
        getPropertyChangeSupport().firePropertyChange("media_stop", null, null);
    }

    @UpnpAction
    public void goMusicNext() {
        getPropertyChangeSupport().firePropertyChange("media_next", null, null);
    }

    @UpnpAction
    public void goMusicPrevious() {
        getPropertyChangeSupport().firePropertyChange("media_previous", null, null);
    }

    @UpnpAction
    public void increaseVolume() {
        getPropertyChangeSupport().firePropertyChange("increasevolume", null, null);
    }

    @UpnpAction
    public void decreaseVolume() {
        getPropertyChangeSupport().firePropertyChange("decreasevolume", null, null);
    }

    @UpnpAction
    public void setVolumeMute(@UpnpInputArgument(name = "NewVolumeMuteValue") boolean newVolumeMuteValue) {
        boolean oldValue = volumeMute;
        volumeMute = newVolumeMuteValue;

        getPropertyChangeSupport().firePropertyChange("mute", oldValue, volumeMute);
    }

    @UpnpAction
    public void takeScreenshot(){
        getPropertyChangeSupport().firePropertyChange("screenshot", null, null);
    }

    @UpnpAction
    public void setShowNotification(@UpnpInputArgument(name = "NewShowNotificationValue") boolean newNotificationValue) {
        boolean oldValue = showNotification;
        showNotification = newNotificationValue;

        getPropertyChangeSupport().firePropertyChange("notification", oldValue, showNotification);
    }

    @UpnpAction
    public void goBack(){
        getPropertyChangeSupport().firePropertyChange("back", null, null);
    }

    @UpnpAction
    public void goHome(){
        getPropertyChangeSupport().firePropertyChange("home", null, null);
    }

    @UpnpAction
    public void goRecentsApps(){
        getPropertyChangeSupport().firePropertyChange("recents", null, null);
    }

    @UpnpAction
    public void setInputKeyboard(@UpnpInputArgument(name = "NewInputKeyboardBValue") String newInputKBValue) {
        //String oldValue = inputKeyboard;
        inputKeyboard = newInputKBValue;

        getPropertyChangeSupport().firePropertyChange("input", null, inputKeyboard);
    }

    @UpnpAction
    public void goDelete(){
        getPropertyChangeSupport().firePropertyChange("delete", null, null);
    }

    @UpnpAction
    public void goSpace(){
        getPropertyChangeSupport().firePropertyChange("space", null, null);
    }

    @UpnpAction
    public void goEnter(){
        getPropertyChangeSupport().firePropertyChange("enter", null, null);
    }

    @UpnpAction
    public void goSearch(){
        getPropertyChangeSupport().firePropertyChange("search", null, null);
    }


    //SCROLL
    @UpnpAction
    public void goScrollUp() {
        getPropertyChangeSupport().firePropertyChange("scroll_up", null, null);
    }
    @UpnpAction
    public void goScrollDown() {
        getPropertyChangeSupport().firePropertyChange("scroll_down", null, null);
    }
    @UpnpAction
    public void goScrollLeft() {
        getPropertyChangeSupport().firePropertyChange("scroll_left", null, null);
    }
    @UpnpAction
    public void goScrollRight() {
        getPropertyChangeSupport().firePropertyChange("scroll_right", null, null);
    }

    //DPAD
    @UpnpAction
    public void goDpadUp() {
        getPropertyChangeSupport().firePropertyChange("dpad_up", null, null);
    }
    @UpnpAction
    public void goDpadDown() {
        getPropertyChangeSupport().firePropertyChange("dpad_down", null, null);
    }
    @UpnpAction
    public void goDpadLeft() {
        getPropertyChangeSupport().firePropertyChange("dpad_left", null, null);
    }
    @UpnpAction
    public void goDpadRight() {
        getPropertyChangeSupport().firePropertyChange("dpad_right", null, null);
    }
    @UpnpAction
    public void goDpadCenter() {
        getPropertyChangeSupport().firePropertyChange("dpad_center", null, null);
    }





}
// DOC:CLASS
