/*
 * Copyright (C) 2010 Google Inc.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ene.smartapp.widget;

import com.ene.smartapp.activities.ControllerDeviceActivity;
import com.ene.smartapp.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

/**
 * Button of the remote controller that has remote controller keycode assigned,
 * and supports displaying highlight with the support of {@link HighlightView}.
 *
 */
public class KeyCodeButton extends ImageButton {

  public final static int SCREENSHOT = 110800;
  public final static int NOTIFICATION = 110801;
  public final static int SCROLL_UP = 110802;
  public final static int SCROLL_DOWN = 110803;
  public final static int SCROLL_LEFT = 110804;
  public final static int SCROLL_RIGHT = 110805;
  public final static int SHOW_KEYBOARD = 110806;
  public final static int APPS_INSTALLEDS = 110807;



  private int keyCode;
  private boolean wasPressed;

  /**
   * Key code handler interface.
   */
  public interface KeyCodeHandler {
    /**
     * Invoked when key has became touched.
     *
     * @param keyCode touched key code.
     */
    public void onTouch(View view, int keyCode);

    /**
     * Invoked when key has been released.
     *
     * @param keyCode released key code.
     */
    public void onRelease(int keyCode);
  }

  public KeyCodeButton(Context context) {
    super(context);
    initialize();
  }

  public KeyCodeButton(Context context, AttributeSet attrs) {
    super(context, attrs);

    TypedArray a = context.obtainStyledAttributes(attrs,
        R.styleable.RemoteButton);

    try {
      CharSequence s = a.getString(R.styleable.RemoteButton_key_code);
      if (s != null) {
        keyCode = valueOfCode(s.toString());
        if(keyCode != -1)
          enableKeyCodeAction();
      } else {
        keyCode = -1;
      }
    } finally {
      a.recycle();
    }

    initialize();
  }

  private void initialize() {
    setScaleType(ScaleType.CENTER_INSIDE);
  }

  private void enableKeyCodeAction() {
    setOnTouchListener(new OnTouchListener() {
      public boolean onTouch(View v, MotionEvent event) {
        Context context = getContext();
        if (context instanceof KeyCodeHandler) {
          KeyCodeHandler handler = (KeyCodeHandler) context;
          switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
              handler.onTouch(v, keyCode);
              break;
            case MotionEvent.ACTION_UP:
              handler.onRelease(keyCode);
              break;
          }
        }

        return false;
      }
    });
  }

  private int valueOfCode(String keyString) {
    switch (keyString) {
      case "KEYCODE_HOME":
        return KeyEvent.KEYCODE_HOME;
      case "KEYCODE_BACK":
        return KeyEvent.KEYCODE_BACK;
      case "KEYCODE_APP_SWITCH":
        return KeyEvent.KEYCODE_APP_SWITCH;
      case "KEYCODE_DPAD_UP":
        return KeyEvent.KEYCODE_DPAD_UP;
      case "KEYCODE_DPAD_DOWN":
        return KeyEvent.KEYCODE_DPAD_DOWN;
      case "KEYCODE_DPAD_LEFT":
        return KeyEvent.KEYCODE_DPAD_LEFT;
      case "KEYCODE_DPAD_RIGHT":
        return KeyEvent.KEYCODE_DPAD_RIGHT;
      case "KEYCODE_DPAD_CENTER":
        return KeyEvent.KEYCODE_DPAD_CENTER;
      case "KEYCODE_VOLUME_UP":
        return KeyEvent.KEYCODE_VOLUME_UP;
      case "KEYCODE_VOLUME_DOWN":
        return KeyEvent.KEYCODE_VOLUME_DOWN;
      case "KEYCODE_POWER":
        return KeyEvent.KEYCODE_POWER;
      case "KEYCODE_TAB":
        return KeyEvent.KEYCODE_TAB;
      case "KEYCODE_SPACE":
        return KeyEvent.KEYCODE_SPACE;
      case "KEYCODE_EXPLORER":
        return KeyEvent.KEYCODE_EXPLORER;
      case "KEYCODE_ENTER":
        return KeyEvent.KEYCODE_ENTER;
      case "KEYCODE_DEL":
        return KeyEvent.KEYCODE_DEL;
      case "KEYCODE_FOCUS":
        return KeyEvent.KEYCODE_FOCUS;
      case "KEYCODE_MENU":
        return KeyEvent.KEYCODE_MENU;
      case "KEYCODE_SEARCH":
        return KeyEvent.KEYCODE_SEARCH;
      case "KEYCODE_MUSIC":
        return KeyEvent.KEYCODE_MUSIC;
      case "KEYCODE_MEDIA_PLAY_PAUSE":
        return KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE;
      case "KEYCODE_MEDIA_STOP":
        return KeyEvent.KEYCODE_MEDIA_STOP;
      case "KEYCODE_MEDIA_NEXT":
        return KeyEvent.KEYCODE_MEDIA_NEXT;
      case "KEYCODE_MEDIA_PREVIOUS":
        return KeyEvent.KEYCODE_MEDIA_PREVIOUS;
      case "KEYCODE_MEDIA_REWIND":
        return KeyEvent.KEYCODE_MEDIA_REWIND;
      case "KEYCODE_MEDIA_FAST_FORWARD":
        return KeyEvent.KEYCODE_MEDIA_FAST_FORWARD;
      case "KEYCODE_MUTE":
        return KeyEvent.KEYCODE_MUTE;
      case "KEYCODE_VOLUME_MUTE":
        return KeyEvent.KEYCODE_VOLUME_MUTE;

      case "SCREENSHOT":
        return SCREENSHOT;
      case "NOTIFICATION":
        return NOTIFICATION;
      case "SCROLL_UP":
        return SCROLL_UP;
      case "SCROLL_DOWN":
        return SCROLL_DOWN;
      case "SCROLL_LEFT":
        return SCROLL_LEFT;
      case "SCROLL_RIGHT":
        return SCROLL_RIGHT;
      case "SHOW_KEYBOARD":
        return SHOW_KEYBOARD;
      case "APPS_INSTALLEDS":
        return APPS_INSTALLEDS;

      default:
        return -1;
    }
  }

  /**
   * Draws button, and notifies highlight view to draw the glow.
   *
   * @see View#onDraw(Canvas)
   */
  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    /*

    // Notify highlight layer to draw highlight
    Context context = getContext();
    if (context instanceof ControllerDeviceActivity) {
      HighlightView highlightView = ((ControllerDeviceActivity) context).getHighlightView();
      if (this.isPressed()) {
        Rect rect = new Rect();
        if (this.getGlobalVisibleRect(rect)) {
          highlightView.drawButtonHighlight(rect);
          wasPressed = true;
        }
      } else if (wasPressed) {
        wasPressed = false;
        highlightView.clearButtonHighlight();
      }
    }
    */
  }
}
