package com.ene.smartapp.utils;

import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;

/**
 * Created by ENESOFTEC on 9/11/2016.
 */

public interface Execute {
    void exec(ActionInvocation action);
    void exec(ActionInvocation action, String description);
    void exec(ActionCallback actionCallback);
}
