package com.ene.smartapp.utils;

import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.meta.Action;

/**
 * Created by ENESOFTEC on 15/10/2016.
 */

public class ActionsInvocations {

    //methods
    private Action music, music_play_pause, music_stop, music_next, music_previous,
            involume, devolume, mute, screenshot, notifications, back, home, recentsApps, keyboard,
            delete, space, enter, search,
            scrollUp, scrollDown, scrollLeft, scrollRight, dpadUp, dpadDown, dpadLeft, dpadRight, dpadCenter,
            move, click,
            appsInstalleds, openapp, uninstall;

    public ActionsInvocations(Action[] actions) {
        for (Action action : actions) {
            switch (action.getName().toLowerCase()) {
                case "gomusic":
                    music = action;
                    break;
                case "gomusicplaypause":
                    music_play_pause = action;
                    break;
                case "gomusicstop":
                    music_stop = action;
                    break;
                case "gomusicnext":
                    music_next = action;
                    break;
                case "gomusicprevious":
                    music_previous = action;
                    break;
                case "increasevolume":
                    involume = action;
                    break;
                case "decreasevolume":
                    devolume = action;
                    break;
                case "setvolumemute":
                    mute = action;
                    break;
                case "takescreenshot":
                    screenshot = action;
                    break;
                case "setshownotification":
                    notifications = action;
                    break;
                case "goback":
                    back = action;
                    break;
                case "gohome":
                    home = action;
                    break;
                case "gorecentsapps":
                    recentsApps = action;
                    break;
                case "setinputkeyboard":
                    keyboard = action;
                    break;
                case "godelete":
                    delete = action;
                    break;
                case "gospace":
                    space = action;
                    break;
                case "goenter":
                    enter = action;
                    break;
                case "gosearch":
                    search = action;
                    break;
                case "goscrollup":
                    scrollUp = action;
                    break;
                case "goscrolldown":
                    scrollDown = action;
                    break;
                case "goscrollleft":
                    scrollLeft = action;
                    break;
                case "goscrollright":
                    scrollRight = action;
                    break;
                case "godpadup":
                    dpadUp = action;
                    break;
                case "godpaddown":
                    dpadDown = action;
                    break;
                case "godpadleft":
                    dpadLeft = action;
                    break;
                case "godpadright":
                    dpadRight = action;
                    break;
                case "godpadcenter":
                    dpadCenter = action;
                    break;
                case "setcoordenatepointer":
                    move = action;
                    break;
                case "clickpointer":
                    click = action;
                    break;

                //Apps Installeds
                case "getappsinstalleds":
                    appsInstalleds = action;
                    break;
                case "setopenapp":
                    openapp = action;
                    break;
                case "setuninstallapp":
                    uninstall = action;
                    break;
            }
        }
    }

    public ActionInvocation getAppsInstalleds(){
        return new ActionInvocation(appsInstalleds);
    }

    public ActionInvocation goMusic(){
        return new ActionInvocation(music);
    }

    public ActionInvocation goMusicPlayPause(){
        return new ActionInvocation(music_play_pause);
    }

    public ActionInvocation goMusicStop(){
        return new ActionInvocation(music_stop);
    }

    public ActionInvocation goMusicNext(){
        return new ActionInvocation(music_next);
    }

    public ActionInvocation goMusicPrevious(){
        return new ActionInvocation(music_previous);
    }

    public ActionInvocation setVolume(boolean value) {
        if(value)
            return new ActionInvocation(involume);
        else
            return new ActionInvocation(devolume);
    }

    public ActionInvocation setVolumeMute(boolean value) {
        return new ActionInvocation(mute,
                new ActionArgumentValue[]{new ActionArgumentValue(mute.getInputArguments()[0], value)});
    }

    public ActionInvocation takeScreenshot() {
        return new ActionInvocation(screenshot);
    }

    public ActionInvocation showNotification(boolean value) {
        return new ActionInvocation(notifications,
                new ActionArgumentValue[]{new ActionArgumentValue(notifications.getInputArguments()[0], value)});
    }

    public ActionInvocation goBack() {
        return new ActionInvocation(back);
    }

    public ActionInvocation goHome() {
        return new ActionInvocation(home);
    }

    public ActionInvocation goRecentsApps() {
        return new ActionInvocation(recentsApps);
    }

    public ActionInvocation inputKeyboard(String value) {
        return new ActionInvocation(keyboard,
                new ActionArgumentValue[]{new ActionArgumentValue(keyboard.getInputArguments()[0], value)});
    }

    public ActionInvocation goDelete() {
        return new ActionInvocation(delete);
    }
    public ActionInvocation goSpace() {
        return new ActionInvocation(space);
    }
    public ActionInvocation goEnter() {
        return new ActionInvocation(enter);
    }
    public ActionInvocation goSearch() {
        return new ActionInvocation(search);
    }

    public ActionInvocation goScrollUp() {
        return new ActionInvocation(scrollUp);
    }
    public ActionInvocation goScrollDown() {
        return new ActionInvocation(scrollDown);
    }
    public ActionInvocation goScrollLeft() {
        return new ActionInvocation(scrollLeft);
    }
    public ActionInvocation goScrollRight() {
        return new ActionInvocation(scrollRight);
    }

    public ActionInvocation goDpadUp() {
        return new ActionInvocation(dpadUp);
    }
    public ActionInvocation goDpadDown() {
        return new ActionInvocation(dpadDown);
    }
    public ActionInvocation goDpadLeft() {
        return new ActionInvocation(dpadLeft);
    }
    public ActionInvocation goDpadRight() {
        return new ActionInvocation(dpadRight);
    }
    public ActionInvocation goDpadCenter() {
        return new ActionInvocation(dpadCenter);
    }

    public ActionInvocation clickMouse() {
        return new ActionInvocation(click);
    }

    public ActionInvocation moveMouse(int x, int y) {
        return new ActionInvocation(move,
                new ActionArgumentValue[]{new ActionArgumentValue(move.getInputArguments()[0], x),
                        new ActionArgumentValue(move.getInputArguments()[1], y)});
    }

    public ActionInvocation openApp(String packageName) {
        return new ActionInvocation(openapp,
                new ActionArgumentValue[]{new ActionArgumentValue(openapp.getInputArguments()[0], packageName)});
    }

    public ActionInvocation uninstallApp(String packageName) {
        return new ActionInvocation(uninstall,
                new ActionArgumentValue[]{new ActionArgumentValue(uninstall.getInputArguments()[0], packageName)});
    }
}