package com.ene.smartapp.utils;

/**
 * Created by ENESOFTEC on 9/11/2016.
 */

public class App {

    private String label;
    private String packageName;

    public App(String label, String packageName){
        this.label = label;
        this.packageName = packageName;
    }

    public String getLabel(){
        return label;
    }

    public String getPackageName(){
        return packageName;
    }

    @Override
    public String toString() {
        return getLabel() + ": " + getPackageName();
    }
}
