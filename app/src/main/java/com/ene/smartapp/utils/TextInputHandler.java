/*
 * Copyright (C) 2009 Google Inc.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ene.smartapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;

import com.ene.smartapp.activities.MainActivity;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;

/**
 * Handles text-related key input.
 *
 * This class also manages a view that displays the last few characters typed.
 *
 */
public final class TextInputHandler {

  private final String TAG = "TextInputHandler";
  private final ActionsInvocations actionsInvocations;

  private final ExecuteAction executeAction;

  /**
   * {@code true} if display should be cleared before next output.
   */
  private boolean clearNextTime;

  public TextInputHandler(ActionsInvocations actions) {
    //this.context = context;
    executeAction = new ExecuteAction();
    this.actionsInvocations = actions;
  }

  /**
   * The view that contains the visual feedback for text input.
   */
  private TextView display;

  /**
   * Handles a character input.
   *
   * @param  c             the character being typed
   * @return {@code true}  if the event was handled
   */
  public boolean handleChar(char c) {
    if (isValidCharacter(c)) {
      String str = String.valueOf(c);
      appendDisplayedText(str);
      executeAction.exec(actionsInvocations.inputKeyboard(str), "inputKeyboard: " + str);
      return true;
    }
    return false;
  }

  /**
   * Handles a key event.
   *
   * @param     event   the key event to handle
   * @return    {@code true} if the event was handled
   */
  public boolean handleKey(KeyEvent event) {
    if (event.getAction() != KeyEvent.ACTION_DOWN) {
      return false;
    }
    int code = event.getKeyCode();
    Log.d(TAG, "keyEvent: " + code + "keySPACE: " + KeyEvent.KEYCODE_SPACE);
    if (code == KeyEvent.KEYCODE_ENTER) {
      displaySingleTimeMessage("ENTER");
      executeAction.exec(actionsInvocations.goEnter(), "ENTER");
      return true;
    }
    if (code == KeyEvent.KEYCODE_DEL) {
      displaySingleTimeMessage("DEL");
      executeAction.exec(actionsInvocations.goDelete(), "DEL");
      return true;
    }

    if (code == KeyEvent.KEYCODE_SPACE) {
      appendDisplayedText(" ");
      executeAction.exec(actionsInvocations.goSpace(), "SPACE");
      return true;
    }

    int c = event.getUnicodeChar();
    return handleChar((char) c);
  }

  private boolean isValidCharacter(int unicode) {
    Log.d(TAG, "character: " + unicode);
    if(unicode == 32) {
      handleKey(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_SPACE));
      return false;
    }else
      return unicode > 0 && unicode < 256;
  }

  public void setDisplay(TextView textView) {
    display = textView;
  }

  private void appendDisplayedText(CharSequence seq) {
    if (display != null) {
      if (!clearNextTime) {
        seq = new StringBuffer(display.getText()).append(seq);
      }
      display.setText(seq);
    }
    clearNextTime = false;
  }

  private void displaySingleTimeMessage(CharSequence seq) {
    if (display != null) {
      display.setText(seq);
      clearNextTime = true;
    }
  }

}
