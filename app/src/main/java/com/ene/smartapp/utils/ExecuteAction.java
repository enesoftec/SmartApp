package com.ene.smartapp.utils;

import android.util.Log;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;

/**
 * Created by ENESOFTEC on 9/11/2016.
 */

public final class ExecuteAction implements Execute {
    private final String TAG = "ExecuteAction";

    private static AndroidUpnpService upnpService = null;

    public static void setUpnpService(AndroidUpnpService service){
        upnpService = service;
    }

    @Override
    public void exec(ActionInvocation action) {
        exec(action, "");
    }

    @Override
    public void exec(ActionInvocation action, String description) {
        if(upnpService == null) return;

        final String txtDescription = description;
        upnpService.getControlPoint().execute(new ActionCallback(action) {
            @Override
            public void success(ActionInvocation actionInvocation) {
                Log.d(TAG, "Success " + txtDescription);
            }

            @Override
            public void failure(ActionInvocation actionInvocation, UpnpResponse upnpResponse, String s) {
                Log.d(TAG, s);
            }
        });
    }

    @Override
    public void exec(ActionCallback actionCallback) {
        if(upnpService == null) return;
        upnpService.getControlPoint().execute(actionCallback);
    }
}
