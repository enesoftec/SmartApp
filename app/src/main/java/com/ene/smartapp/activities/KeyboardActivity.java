/*
 * Copyright (C) 2009 Google Inc.  All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ene.smartapp.activities;

import com.ene.smartapp.R;
import com.ene.smartapp.utils.ActionsInvocations;
import com.ene.smartapp.utils.ExecuteAction;
import com.ene.smartapp.utils.TextInputHandler;
import com.ene.smartapp.utils.TouchHandler;
import com.ene.smartapp.utils.TouchHandler.Mode;
import com.ene.smartapp.widget.ImeInterceptView;
import com.ene.smartapp.widget.KeyCodeButton;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.meta.Device;

/**
 * Text input activity.
 * <p>
 * Displays a soft keyboard if needed.
 *
 */
public final class KeyboardActivity extends AppCompatActivity  implements KeyCodeButton.KeyCodeHandler{

  private final String TAG = "KeyboardActivity";

  /**
   * Captures text inputs.
   */
  private final TextInputHandler textInputHandler;

  /**
   * The main view.
   */
  private ImeInterceptView view;
  private ActionsInvocations actions;
  private ExecuteAction executeAction;


  public KeyboardActivity() {
    Device device = MainActivity.deviceSelected;

    executeAction = new ExecuteAction();

    if(device != null && device.getServices()[0].hasActions()) {
       actions = new ActionsInvocations(device.getServices()[0].getActions());
      textInputHandler = new TextInputHandler(actions);
    }else
      textInputHandler = null;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.keyboard);

    view = (ImeInterceptView) findViewById(R.id.keyboard);
    view.requestFocus();
    view.setInterceptor(new ImeInterceptView.Interceptor() {
      public boolean onKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
          switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_BACK:
              finish();
              return true;
          }
        }
        return textInputHandler.handleKey(event);
      }

      public boolean onSymbol(char c) {
        textInputHandler.handleChar(c);
        return false;
      }
    });

    textInputHandler.setDisplay(
        (TextView) findViewById(R.id.text_feedback_chars));

    new TouchHandler(view, Mode.POINTER, actions);
  }

  @Override
  public boolean onTrackballEvent(MotionEvent event){
    if (event.getAction() == MotionEvent.ACTION_DOWN){
      finish();
    }
    return super.onTrackballEvent(event);
  }


  @Override
  public void onTouch(View v, int keyCode) {
    if(actions != null) {
      String button = "";
      ActionInvocation actionInvocation = null;

      switch (keyCode) {
        case KeyEvent.KEYCODE_SEARCH:
          actionInvocation = actions.goSearch();
          button = "SEARCH";
          break;
      }

      if(actionInvocation != null){
        executeAction.exec(actionInvocation, button);
      }
    }
  }

  @Override
  public void onRelease(int keyCode) {

  }
}
