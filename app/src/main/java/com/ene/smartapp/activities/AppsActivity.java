package com.ene.smartapp.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.ene.smartapp.R;
import com.ene.smartapp.utils.ActionsInvocations;
import com.ene.smartapp.utils.App;
import com.ene.smartapp.utils.Apps;
import com.ene.smartapp.utils.ExecuteAction;
import com.ene.smartapp.utils.ListAdapter;

import org.teleal.cling.model.meta.Device;

import java.util.ArrayList;

/**
 * Created by ENESOFTEC on 9/11/2016.
 */

public class AppsActivity extends AppCompatActivity {

    private final String TAG = "AppsActivity";

    private ExecuteAction executeAction;
    private ActionsInvocations actions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps);

        ListView listView = (ListView) findViewById(R.id.listview_apps);


        Bundle bundle = getIntent().getExtras();
        if(bundle == null) finish();

        executeAction = new ExecuteAction();
        Device device = MainActivity.deviceSelected;
        if(device != null && device.getServices()[0].hasActions()) {
            actions = new ActionsInvocations(device.getServices()[0].getActions());
        }

        final Apps apps = new Apps();
        apps.setAppsFromString(bundle.getString("source"));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                executeAction.exec(actions.openApp(apps.getApp(position).getPackageName()));
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int pos = position;
                new AlertDialog.Builder(AppsActivity.this)
                        .setMessage("¿Estás seguro que deseas desinstalar esta aplicación?")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                executeAction.exec(actions.uninstallApp(apps.getApp(pos).getPackageName()), "UNINSTALL");
                            }
                        })
                        .setNegativeButton("No", null)
                        .setCancelable(false)
                        .show();
                return false;
            }
        });

        listView.setAdapter(new ListAdapter(this, android.R.layout.simple_list_item_2, apps.getApps()) {
            @Override
            public void onEntrada(Object entrada, View view, int position) {
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                text1.setTextColor(getResources().getColor(android.R.color.white));
                text2.setTextColor(getResources().getColor(android.R.color.white));

                App app = (App)entrada;

                text1.setText(app.getLabel());
                text2.setText(app.getPackageName());

                Log.d(TAG, app.toString());
            }
        });
    }
}
