package com.ene.smartapp.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;

import com.ene.smartapp.R;
import com.ene.smartapp.utils.BrowserUpnpService;
import com.ene.smartapp.utils.ExecuteAction;

import java.util.Comparator;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private View vParent;
    private ImageView btnScan, imageView;

    private Point point;
    private Handler mHandler;
    private WindowManager.LayoutParams layoutParams;
    public WindowManager windowManager;
    private ImageView pointer;


    private float initVerticalSlider1, initVerticalSlider2, posVertical;

    private boolean searching = false;

    private final String TAG = "MainActivity";

    // Cling
    private ArrayAdapter listAdapter;

    private BrowseRegistryListener registryListener = new BrowseRegistryListener();

    public static AndroidUpnpService upnpService;

    public static Device deviceSelected = null;



    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            upnpService = (AndroidUpnpService) service;

            // Refresh the list with all known devices
            listAdapter.clear();
            for (Device device : upnpService.getRegistry().getDevices()) {
                registryListener.deviceAdded(device);
            }

            // Getting ready for future device advertisements
            upnpService.getRegistry().addListener(registryListener);

            // Search asynchronously for all devices
            //upnpService.getControlPoint().search();
        }

        public void onServiceDisconnected(ComponentName className) {
            upnpService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.lvDevices);
        vParent = findViewById(R.id.vgParentVerticalSlider);
        btnScan = (ImageView) findViewById(R.id.btnScan);
        imageView = (ImageView) findViewById(R.id.ivBackground);


        btnScan.setOnTouchListener(touchDragDropVerticalSlider());
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Empty
            }
        });


        vParent.post(new Runnable() {
            @Override
            public void run() {
                initVerticalSlider1 = btnScan.getY();
                initVerticalSlider2 = imageView.getY();
            }
        });
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        listAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DeviceDisplay device = (DeviceDisplay) listAdapter.getItem(position);
                deviceSelected = device.getDevice();
                ExecuteAction.setUpnpService(upnpService);

                Intent intent = new Intent(MainActivity.this, ControllerDeviceActivity.class);
                startActivity(intent);
            }
        });

        bindService(
                new Intent(this, BrowserUpnpService.class),
                serviceConnection,
                Context.BIND_AUTO_CREATE
        );


    }

    protected void searchNetwork() {
        if (upnpService == null) return;

        Toast.makeText(this, "Buscando Dispositivos", Toast.LENGTH_SHORT).show();
        upnpService.getRegistry().removeAllRemoteDevices();
        upnpService.getControlPoint().search();
    }


    protected class BrowseRegistryListener extends DefaultRegistryListener {

        /* Discovery performance optimization for very slow Android devices! */

        @Override
        public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice device) {
            deviceAdded(device);
        }

        @Override
        public void remoteDeviceDiscoveryFailed(Registry registry, final RemoteDevice device, final Exception ex) {
            /*showToast(
                    "Discovery failed of '" + device.getDisplayString() + "': " +
                            (ex != null ? ex.toString() : "Couldn't retrieve device/service descriptors"),
                    true
            );*/
            deviceRemoved(device);
        }
        /* End of optimization, you can remove the whole block if your Android handset is fast (>= 600 Mhz) */

        @Override
        public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
            deviceAdded(device);
        }

        @Override
        public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
            deviceRemoved(device);
        }

        @Override
        public void localDeviceAdded(Registry registry, LocalDevice device) {
            deviceAdded(device);
        }

        @Override
        public void localDeviceRemoved(Registry registry, LocalDevice device) {
            deviceRemoved(device);
        }

        public void deviceAdded(final Device device) {
            runOnUiThread(new Runnable() {
                public void run() {
                    DeviceDisplay d = new DeviceDisplay(device);

                    if(!d.getDevice().getType().getDisplayString().toLowerCase().equals("smartapptv"))
                        return;

                    int position = listAdapter.getPosition(d);
                    if (position >= 0) {
                        // Device already in the list, re-set new value at same position
                        listAdapter.remove(d);
                        listAdapter.insert(d, position);
                    } else {
                        listAdapter.add(d);
                    }


                    System.out.println("HEEEREE: " + d.getDevice().getServices()[0].getActions().length);



                    // Sort it?
                    // listAdapter.sort(DISPLAY_COMPARATOR);
                    // listAdapter.notifyDataSetChanged();
                }
            });
        }

        public void deviceRemoved(final Device device) {
            runOnUiThread(new Runnable() {
                public void run() {
                    listAdapter.remove(new DeviceDisplay(device));
                }
            });
        }
    }

    protected void showToast(final String msg, final boolean longLength) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(
                        MainActivity.this,
                        msg,
                        longLength ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT
                ).show();
            }
        });
    }

    protected class DeviceDisplay {

        Device device;

        public DeviceDisplay(Device device) {
            this.device = device;
        }

        public Device getDevice() {
            return device;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DeviceDisplay that = (DeviceDisplay) o;
            return device.equals(that.device);
        }

        @Override
        public int hashCode() {
            return device.hashCode();
        }

        @Override
        public String toString() {
            String name =
                    device.getDetails() != null && device.getDetails().getFriendlyName() != null
                            ? device.getDetails().getFriendlyName()
                            : device.getDisplayString();
            // Display a little star while the device is being loaded (see performance optimization earlier)
            return device.isFullyHydrated() ? name : name + " *";
        }
    }

    static final Comparator<DeviceDisplay> DISPLAY_COMPARATOR =
            new Comparator<DeviceDisplay>() {
                public int compare(DeviceDisplay a, DeviceDisplay b) {
                    return a.toString().compareTo(b.toString());
                }
            };



    @Override
    protected void onDestroy() {
        super.onDestroy();

    }



    private View.OnTouchListener touchDragDropVerticalSlider(){
        final int ratio = 100;

        View.OnTouchListener touch = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                PointF pfStart1 = new PointF(view.getX(), view.getY());
                PointF pfStart2 = new PointF(imageView.getX(), imageView.getY());
                PointF pfDown1 = new PointF(), pfDown2 = new PointF();

                switch (motionEvent.getAction()){

                    case MotionEvent.ACTION_DOWN:
                        pfDown1.y = motionEvent.getY();
                        pfDown2.y = motionEvent.getY();

                        searching = false;

                        break;

                    case MotionEvent.ACTION_MOVE:

                        PointF moving1 = new PointF(motionEvent.getX() - pfDown1.x, motionEvent.getY() - pfDown1.y);
                        PointF moving2 = new PointF(motionEvent.getX() - pfDown2.x, motionEvent.getY() - pfDown2.y);

                        posVertical = pfStart1.y + moving1.y - ratio;

                        boolean upping = true;

                        float percent = (posVertical * 100) / initVerticalSlider1;

                        if(pfStart1.y < posVertical) {
                            System.out.println("Estas Bajandooo");
                            if(percent > 100)
                                upping = false;
                        }else {
                            System.out.println("Estas Subiendoo");
                            if(!searching && percent < 70){
                                //Start Scan
                                searchNetwork();

                                view.setY(initVerticalSlider1);
                                imageView.setY(initVerticalSlider2);

                                searching = true;
                                return false;
                            }
                        }
                        if(upping && !searching) {
                            view.setY(posVertical);
                            imageView.setY(pfStart2.y + moving2.y - ratio);
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        view.setY(initVerticalSlider1);
                        imageView.setY(initVerticalSlider2);

                        searching = false;
                        break;
                }

                vParent.invalidate();
                return false;
            }
        };

        return touch;
    }




}
