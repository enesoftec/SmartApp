package com.ene.smartapp.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.ene.smartapp.R;
import com.ene.smartapp.utils.ActionsInvocations;
import com.ene.smartapp.utils.Execute;
import com.ene.smartapp.utils.ExecuteAction;
import com.ene.smartapp.utils.TextInputHandler;
import com.ene.smartapp.widget.ImeInterceptView;
import com.ene.smartapp.widget.KeyCodeButton;
import com.ene.smartapp.widget.SoftDpad;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Device;

public class ControllerDeviceActivity extends AppCompatActivity implements
        KeyCodeButton.KeyCodeHandler {

    private final String TAG = "ControllerDevice";

    private ActionsInvocations myActions = null;

    private boolean showed_notification = false;
    private boolean volume_muted = false;
    private boolean isPlaying = false;
    private KeyCodeButton musicPlayPause;

    private ExecuteAction executeAction;
    private ProgressDialog progressExecuteAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller_device);

        SoftDpad softDpad = (SoftDpad) findViewById(R.id.SoftDpad);
        softDpad.setDpadListener(getDefaultDpadListener());

        Device mDevice = MainActivity.deviceSelected;
        executeAction = new ExecuteAction();


        if(mDevice != null && mDevice.getServices()[0].hasActions()) {
            myActions = new ActionsInvocations(mDevice.getServices()[0].getActions());
        }

        progressExecuteAction = new ProgressDialog(this);
        progressExecuteAction.setMessage("Ejecutando Acción...");
        progressExecuteAction.setCancelable(false);
    }

    private SoftDpad.DpadListener getDefaultDpadListener(){
        return new SoftDpad.DpadListener() {
            @Override
            public void onDpadClicked() {
                if(myActions != null){
                    executeAction.exec(myActions.goDpadCenter());
                }
            }

            @Override
            public void onDpadMoved(SoftDpad.Direction direction, boolean pressed) {
                if(myActions != null && pressed){
                    ActionInvocation actionInvocation = null;
                    switch (direction){
                        case UP:
                            actionInvocation = myActions.goDpadUp();
                            break;
                        case DOWN:
                            actionInvocation = myActions.goDpadDown();
                            break;
                        case LEFT:
                            actionInvocation = myActions.goDpadLeft();
                            break;
                        case RIGHT:
                            actionInvocation = myActions.goDpadRight();
                            break;
                    }

                    if(actionInvocation != null){
                        executeAction.exec(actionInvocation);
                    }

                    Log.d(TAG, direction.toString());
                }
            }
        };
    }

    @Override
    public void onTouch(View v, int keyCode) {
        if(myActions != null){
            boolean withOutput = false;
            String button = "";
            ActionInvocation actionInvocation = null;

            switch (keyCode){
                case KeyEvent.KEYCODE_BACK:
                    button = "BACK";
                    actionInvocation = myActions.goBack();
                    break;
                case KeyEvent.KEYCODE_HOME:
                    actionInvocation = myActions.goHome();
                    button = "HOME";
                    break;
                case KeyEvent.KEYCODE_APP_SWITCH:
                    actionInvocation = myActions.goRecentsApps();
                    button = "APPS_RECENTS";
                    break;

                case KeyEvent.KEYCODE_MUSIC:
                    actionInvocation = myActions.goMusic();
                    button = "MUSIC";
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    musicPlayPause = (KeyCodeButton)v;
                    isPlaying = !isPlaying;
                    if(isPlaying)
                        musicPlayPause.setImageResource(R.drawable.ic_pause_selector);
                    else
                        musicPlayPause.setImageResource(R.drawable.ic_play_selector);
                    actionInvocation = myActions.goMusicPlayPause();
                    withOutput = true;
                    button = "MEDIA_PLAY_PAUSE";
                    break;
                case KeyEvent.KEYCODE_MEDIA_STOP:
                    if(musicPlayPause != null)
                        musicPlayPause.setImageResource(R.drawable.ic_play_selector);
                    isPlaying = false;
                    actionInvocation = myActions.goMusicStop();
                    button = "MEDIA_STOP";
                    break;
                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    actionInvocation = myActions.goMusicNext();
                    button = "MEDIA_NEXT";
                    break;
                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    actionInvocation = myActions.goMusicPrevious();
                    button = "MEDIA_PREVIOUS";
                    break;

                case KeyEvent.KEYCODE_VOLUME_UP:
                    actionInvocation = myActions.setVolume(true);
                    button = "VOLUME_UP";
                    break;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    actionInvocation = myActions.setVolume(false);
                    button = "VOLUME_DOWN";
                    break;
                case KeyEvent.KEYCODE_VOLUME_MUTE:
                    volume_muted = !volume_muted;
                    actionInvocation = myActions.setVolumeMute(volume_muted);
                    button = "VOLUME_MUTE";
                    break;

                //Custom
                case KeyCodeButton.SCREENSHOT:
                    actionInvocation = myActions.takeScreenshot();
                    button = "SCREENSHOT";
                    break;
                case KeyCodeButton.NOTIFICATION:
                    showed_notification = !showed_notification;
                    actionInvocation = myActions.showNotification(showed_notification);
                    button = "NOTIFICATION";
                    break;
                case KeyCodeButton.SCROLL_UP:
                    actionInvocation = myActions.goScrollUp();
                    button = "SCROLL_UP";
                    break;
                case KeyCodeButton.SCROLL_DOWN:
                    actionInvocation = myActions.goScrollDown();
                    button = "SCROLL_DOWN";
                    break;
                case KeyCodeButton.SCROLL_LEFT:
                    actionInvocation = myActions.goScrollLeft();
                    button = "SCROLL_LEFT";
                    break;
                case KeyCodeButton.SCROLL_RIGHT:
                    actionInvocation = myActions.goScrollRight();
                    button = "SCROLL_RIGHT";
                    break;
                case KeyCodeButton.SHOW_KEYBOARD:
                    startActivity(new Intent(this, KeyboardActivity.class));
                    break;

                case KeyCodeButton.APPS_INSTALLEDS:
                    actionInvocation = myActions.getAppsInstalleds();
                    button = "APPS_INSTALLEDS";
                    withOutput = true;
                    break;
            }
            if(actionInvocation != null){
                if(withOutput) {
                    progressExecuteAction.show();
                    executeAction.exec(new ActionCallback(actionInvocation) {
                        @Override
                        public void success(ActionInvocation actionInvocation) {
                            progressExecuteAction.dismiss();
                            switch (actionInvocation.getAction().getName().toLowerCase()){
                                case "getappsinstalleds":
                                    Intent intent = new Intent(ControllerDeviceActivity.this,
                                            AppsActivity.class);
                                    intent.putExtra("source", actionInvocation.getOutput()[0].getValue().toString());
                                    startActivity(intent);
                                    break;
                            }
                        }

                        @Override
                        public void failure(ActionInvocation actionInvocation, UpnpResponse upnpResponse, String s) {
                            progressExecuteAction.dismiss();
                            switch (actionInvocation.getAction().getName().toLowerCase()){
                                case "getappsinstalleds":
                                    Log.d(TAG, s);
                                    break;
                                case "gomusicplaypause":
                                    isPlaying = false;
                                    break;
                            }

                        }
                    });
                }else
                    executeAction.exec(actionInvocation, button);
            }
        }
    }

    @Override
    public void onRelease(int keyCode) {

    }

    protected void showToast(final String msg, final boolean longLength) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(
                        ControllerDeviceActivity.this,
                        msg,
                        longLength ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT
                ).show();
            }
        });
    }


/*
    public HighlightView getHighlightView() {
        return surface;
    }
*/
}
